#!/usr/bin/env node
import "source-map-support/register";

import * as cdk from "@aws-cdk/core";

import { EcsStack } from "./lib/stack/ecs-stack";
import { VpcStack } from "./lib/stack/vpc-stack";
import { StaticsiteStack } from "./lib/stack/staticsite-stack";

const app = new cdk.App();

const appName = app.node.tryGetContext("app_name");
const mode = app.node.tryGetContext("mode");
const name = `${appName}-${mode}`;

if (!appName) {
  throw new Error("`appName` is not defined");
}
if (!mode) {
  throw new Error("`mode` is not defined");
}

const vpcStck = new VpcStack(app, `vpc-${appName}`, {
  env: {
    account: "329855332982",
    region: "ap-northeast-1",
  },
});

// const ecsStck = new EcsStack(app, `ecsstack-${name}`, {
//   env: {
//     account: "329855332982",
//     region: "ap-northeast-1",
//   },
//   vpc: vpcStck.vpc
// });

// const staticsiteStack = new StaticsiteStack(app, `staticsite-${appName}`, {
//   env: {
//     account: "329855332982",
//     region: "ap-northeast-1",
//   },
//   vpc: vpcStck.vpc!
// });

app.synth();
