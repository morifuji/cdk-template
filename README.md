CDKのテンプレートです。

スタックは3つ。main.tsで各stackを呼び出しているのでPJごとに適宜修正を

- vpc
- ecs
- static-site

## How to use

```bash
# コンテキストを念のため目視チェック
$ cdk context

# コンテキストをテンプレから修正
$ cp cdk.context.json.sample cdk.context.json
$ vi cdk.context.json

# アカウントを取得してmain.tsに設定
$ aws sts get-caller-identity

# diff確認
$ cdk diff 
# deploy
$ cdk deploy

```

## その他

#### キーペアどうやって作成する？

```bash
# pub/private作成
# pubをEC2>キーペア>キーペアのインポートからインポート
$ ssh-keygen -b 2048 -t rsa

# SSHできるよう権限付与しておく
$ chmod 500 ./cdktest-gateway
```


## RemovalPolicy

CDKが失敗した際に、ほぼ全てのリソースはロールバックするが、一部のリソースは自動でロールバックしない

- RDS
- LogGroup
- S3

初回デプロイ後には、 RETAINに必ず変更すること

```diff
- removalPolicy: cdk.RemovalPolicy.DESTROY,
+ removalPolicy: cdk.RemovalPolicy.RETAIN,
```
