import * as cdk from '@aws-cdk/core';
import ec2 = require("@aws-cdk/aws-ec2");
import ecs = require("@aws-cdk/aws-ecs");
import ecs_patterns = require("@aws-cdk/aws-ecs-patterns");
import logs = require('@aws-cdk/aws-logs')

export interface EcsProps {
  vpc: ec2.IVpc;
}

export class Ecs extends cdk.Construct {

  ecsSecurityGroup: ec2.ISecurityGroup|null = null

  constructor(scope: cdk.Construct, id: string, props: EcsProps) {
    super(scope, id);

    const cluster = new ecs.Cluster(this, `ecs-cluster-${id}`, {
      vpc: props.vpc
    });

    const logGroup = new logs.LogGroup(this, `loggroup-${id}`, {
      logGroupName: `/ecs/task-definition-${id}`,
      removalPolicy: cdk.RemovalPolicy.DESTROY
    })
    const logDriver = new ecs.AwsLogDriver({
      streamPrefix: 'ecs',
      logGroup: logGroup,
    })

    const mode = this.node.tryGetContext("mode")
    if (!mode) {
      throw new Error("`mode` is not defined")
    }


    const appSecurityGroup = new ec2.SecurityGroup(this, `securitygroup-app-${id}`, {
      securityGroupName: `app-${id}`,
      vpc: props.vpc
  })

    // Create a load-balanced Fargate service and make it public
    const service = new ecs_patterns.ApplicationLoadBalancedFargateService(this, `ecs-service-${id}`, {
      cluster: cluster, // Required
      cpu: 512,
      desiredCount: 1, // Default is 1
      taskImageOptions: {
        // イメージはCIで設定するのでここでは適当なイメージ
        image: ecs.ContainerImage.fromRegistry("nginx:latest"),
        containerPort: 80,
        enableLogging: true,
        logDriver: logDriver,
        environment: {
          // 環境変数
        },
      },
      memoryLimitMiB: 1024, // Default is 512
      publicLoadBalancer: true,
      assignPublicIp: false,
      securityGroups: [appSecurityGroup]
    });

    const target = service.service.autoScaleTaskCount({
      minCapacity: 1,
      maxCapacity: 4
    })

    target.scaleOnCpuUtilization(`cpu-scaling-${id}`, {
      targetUtilizationPercent: 75,
    });
    target.scaleOnMemoryUtilization(`memory-scaling-${id}`, {
      targetUtilizationPercent: 75,
    });

    this.ecsSecurityGroup = appSecurityGroup
  }
}
