#!/usr/bin/env node
import cloudfront = require('@aws-cdk/aws-cloudfront');
import s3 = require('@aws-cdk/aws-s3');
import s3deploy = require('@aws-cdk/aws-s3-deployment');
import cdk = require('@aws-cdk/core');
import route53 = require('@aws-cdk/aws-route53')
import { Construct } from '@aws-cdk/core';

export interface StaticsiteProps {
    domainName: string;
    siteSubDomain: string;
}

/**
 * Static site infrastructure, which deploys site content to an S3 bucket.
 *
 * The site redirects from HTTP to HTTPS, using a CloudFront distribution,
 * Route53 alias record, and ACM certificate.
 */
export class Staticsite extends Construct {
    constructor(parent: Construct, name: string, props: StaticsiteProps) {
        super(parent, name);

        // const zone = route53.HostedZone.fromLookup(this, 'Zone', { domainName: props.domainName });
        // const siteDomain = props.siteSubDomain === null 
        // ? props.domainName
        // : props.siteSubDomain + '.' + props.domainName;

        // Content bucket
        const siteBucket = new s3.Bucket(this, `SiteBucket-${name}`, {
            bucketName: name,
            websiteIndexDocument: 'index.html',
            websiteErrorDocument: 'index.html',
            publicReadAccess: true,

            // The default removal policy is RETAIN, which means that cdk destroy will not attempt to delete
            // the new bucket, and it will remain in your account until manually deleted. By setting the policy to
            // DESTROY, cdk destroy will attempt to delete the bucket, but will error if the bucket is not empty.
            // NOT recommended for production code
            removalPolicy: cdk.RemovalPolicy.DESTROY, 

            websiteRoutingRules: [
                {
                    condition: {
                        httpErrorCodeReturnedEquals: "404",
                    },
                    httpRedirectCode: "301",
                    replaceKey: s3.ReplaceKey.with("redirect.html")
                }
            ]
        });
        new cdk.CfnOutput(this, 'Bucket', { value: siteBucket.bucketName });

        // // // TLS certificate
        // const certificateArn = new acm.DnsValidatedCertificate(this, 'SiteCertificate', {
        //     domainName: siteDomain,
        //     hostedZone: zone
        // }).certificateArn;
        // new cdk.CfnOutput(this, 'Certificate', { value: certificateArn });

        // CloudFront distribution that provides HTTPS
        const distribution = new cloudfront.CloudFrontWebDistribution(this, `SiteDistribution-${name}`, {
            // aliasConfiguration: {
            //     acmCertRef: certificateArn,
            //     names: [ siteDomain ],
            //     sslMethod: cloudfront.SSLMethod.SNI,
            //     securityPolicy: cloudfront.SecurityPolicyProtocol.TLS_V1_1_2016,
            // },
            errorConfigurations: [
                {
                    errorCode: 403,
                    responseCode: 200,
                    responsePagePath: "/index.html"
                },
                {
                    errorCode: 404,
                    responseCode: 200,
                    responsePagePath: "/index.html"
                }
            ],
            originConfigs: [
                {
                    s3OriginSource: {
                        s3BucketSource: siteBucket
                    },
                    behaviors : [ {isDefaultBehavior: true}],
                }
            ]
        });
        new cdk.CfnOutput(this, 'DistributionId', { value: distribution.distributionId });

        // サイトURL表示
        new cdk.CfnOutput(this, 'SiteUrl', { value: 'https://' + distribution.domainName });


        // // // Route53 alias record for the CloudFront distribution
        // new route53.ARecord(this, 'SiteAliasRecord', {
        //     recordName: siteDomain,
        //     target: route53.AddressRecordTarget.fromAlias(new targets.CloudFrontTarget(distribution)),
        //     zone
        // });

        const staticFilePath = this.node.tryGetContext('static_file_path');

        // Deploy site contents to S3 bucket
        new s3deploy.BucketDeployment(this, `DeployWithInvalidation-${name}`, {
            sources: [ s3deploy.Source.asset(staticFilePath) ],
            destinationBucket: siteBucket,
            distribution,
            distributionPaths: ['/*'],
          });
    }
}
