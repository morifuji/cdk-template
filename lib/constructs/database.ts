import * as cdk from "@aws-cdk/core";
import rds = require("@aws-cdk/aws-rds");
import ec2 = require("@aws-cdk/aws-ec2");

export interface RdsStackProps {
  vpc: ec2.IVpc;
  appSecurityGroup: ec2.ISecurityGroup;
}

export class Database extends cdk.Construct {
  constructor(scope: cdk.Construct, id: string, props: RdsStackProps) {
    super(scope, id);

    const rdsEngine = rds.DatabaseInstanceEngine.mysql({
      version: rds.MysqlEngineVersion.VER_5_7_31,
    });

    // Set open cursors with parameter group
    const parameterGroup = new rds.ParameterGroup(
      this,
      `parameter-group-${id}`,
      {
        engine: rdsEngine,
        parameters: {
          character_set_client: "utf8mb4",
          character_set_connection: "utf8mb4",
          character_set_database: "utf8mb4",
          character_set_results: "utf8mb4",
          character_set_server: "utf8mb4",
          "skip-character-set-client-handshake": "1",
          time_zone: "Asia/Tokyo",
        },
      }
    );

    const securityGroup = new ec2.SecurityGroup(
      this,
      `securitygroup-rds-${id}`,
      {
        securityGroupName: `rds-${id}`,
        vpc: props.vpc,
      }
    );

    const gatewayserverSecuritygroupId = this.node.tryGetContext(
      "gatewayserver_securitygroup_id"
    );
    if (!gatewayserverSecuritygroupId) {
      throw new Error("`gatewayserver_securitygroup_id` is not defined");
    }
    const gatewayserverSecurityGroup = ec2.SecurityGroup.fromSecurityGroupId(
      this,
      `gatewayserversecuritygroup-${id}`,
      gatewayserverSecuritygroupId
    );

    securityGroup.addIngressRule(
      gatewayserverSecurityGroup,
      ec2.Port.tcp(3306)
    );
    securityGroup.addIngressRule(props.appSecurityGroup, ec2.Port.tcp(3306));
    securityGroup.addEgressRule(ec2.Peer.anyIpv4(), ec2.Port.allTcp());

    const masterPassword = this.node.tryGetContext("rds_master_password");
    if (!masterPassword) {
      throw new Error("`rds_master_password` is not defined");
    }

    const instance = new rds.DatabaseInstance(this, `rds-${id}`, {
      engine: rdsEngine,
      instanceType: ec2.InstanceType.of(
        ec2.InstanceClass.T2,
        ec2.InstanceSize.MICRO
      ),
      credentials: rds.Credentials.fromUsername("admin", {
        password: masterPassword,
      }),
      vpc: props.vpc,
      maxAllocatedStorage: 200,
      parameterGroup: parameterGroup,
      securityGroups: [securityGroup],
      multiAz: false,
      allocatedStorage: 30,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    new cdk.CfnOutput(this, `rds-endpoint ${id}`, {
      value: instance.instanceEndpoint.hostname,
    });
  }
}
