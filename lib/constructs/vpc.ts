import * as cdk from '@aws-cdk/core';
import ec2 = require("@aws-cdk/aws-ec2");

export class Vpc extends cdk.Construct {

  public readonly vpc: ec2.IVpc

  constructor(scope: cdk.Construct, id: string) {
    super(scope, id);
    const vpc = new ec2.Vpc(this, `vpc-${id}`, {
      maxAzs: 2, // Default is all AZs in region
      natGateways: 0
    });

    this.vpc = vpc
  }
}
