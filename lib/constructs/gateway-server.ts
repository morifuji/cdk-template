import * as cdk from "@aws-cdk/core";
import ec2 = require("@aws-cdk/aws-ec2");
import { Ec2Service } from "@aws-cdk/aws-ecs";

export interface GatewayServerProps {
  vpc: ec2.IVpc;
}

export class GatewayServer extends cdk.Construct {

  gatewayserver: ec2.IInstance

  constructor(scope: cdk.Construct, id: string, props: GatewayServerProps) {
    super(scope, id);

    const cidr = this.node.tryGetContext("gatewayserver_allowed_cidr");
    if (!cidr) {
      throw new Error("`gatewayserver_allowed_cidr` is not defined");
    }

    const gatewayserverSecurityGroup = new ec2.SecurityGroup(
      this,
      `securitygroup-${id}`,
      {
        vpc: props.vpc,
      }
    );

    gatewayserverSecurityGroup.addIngressRule(
      ec2.Peer.ipv4(cidr),
      ec2.Port.tcp(22),
      "from cdk"
    );

    gatewayserverSecurityGroup.addIngressRule(
      ec2.Peer.ipv4(cidr),
      ec2.Port.allTcp(),
      "from cdk"
    );

    gatewayserverSecurityGroup.addEgressRule(
      ec2.Peer.anyIpv4(),
      ec2.Port.allTcp(),
      "from cdk"
    );

    const image = new ec2.AmazonLinuxImage({
      generation: ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
    });

    const keyName = this.node.tryGetContext("gatewayserver_key_name");
    if (!keyName) {
      throw new Error("`gatewayserver_key_name` is not defined");
    }

    const instance = new ec2.Instance(this, `gateway-${id}`, {
      vpc: props.vpc,
      vpcSubnets: {
        subnets: [props.vpc.publicSubnets[0]]
      },
      instanceType: ec2.InstanceType.of(
        ec2.InstanceClass.T2,
        ec2.InstanceSize.MICRO
      ),
      machineImage: image,
      securityGroup: gatewayserverSecurityGroup,
      keyName: keyName,
    });

    const eip = new ec2.CfnEIP(this, `CfnEIP-${id}`, {
      domain: "vpc",
      instanceId: instance.instanceId,
    });

    this.gatewayserver = instance
  }
}
