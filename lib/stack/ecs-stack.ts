import * as cdk from '@aws-cdk/core';
import ec2 = require("@aws-cdk/aws-ec2");

import { Database } from '../constructs/database'
import { Ecs } from '../constructs/ecs'

export class EcsStack extends cdk.Stack {

  constructor(scope: cdk.Construct, id: string, props: cdk.StackProps & { vpc: ec2.IVpc }) {
    super(scope, id, props);

    // ecs
    const ecs = new Ecs(this, `ecs-${id}`, {
      vpc: props.vpc
    })

    // database
    new Database(this, `database-${id}`, {
      vpc: props.vpc,
      appSecurityGroup: ecs.ecsSecurityGroup!!
    })
  }
}
