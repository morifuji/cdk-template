import * as cdk from "@aws-cdk/core";
import ec2 = require("@aws-cdk/aws-ec2");

import { Database } from "../constructs/database";
import { Ecs } from "../constructs/ecs";

import { Staticsite } from "../constructs/staticsite";

export class StaticsiteStack extends cdk.Stack {
  constructor(parent: cdk.App, name: string, props: cdk.StackProps & { vpc: ec2.IVpc }) {
    super(parent, name, props);

    const domainName = this.node.tryGetContext("domain_name");
    const siteSubDomain = this.node.tryGetContext("site_sub_domain");

    new Staticsite(this, name, {
      domainName,
      siteSubDomain,
    });
  }
}
