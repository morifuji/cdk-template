import * as cdk from '@aws-cdk/core';
import { Vpc } from "./../constructs/vpc"
import { GatewayServer } from "./../constructs/gateway-server"
import ec2 = require("@aws-cdk/aws-ec2");

export class VpcStack extends cdk.Stack {

  vpc: ec2.IVpc
  gateway: ec2.IInstance

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vpc = new Vpc(this, `vpc-${id}`)
    
    const gateway = new GatewayServer(this, id, {
      vpc: vpc.vpc
    })

    this.vpc = vpc.vpc
    this.gateway = gateway.gatewayserver
  }
}
